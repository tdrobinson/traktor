package traktor

import movies._
import scala.util.{Failure, Success, Properties}

import util.Logging

import unfiltered.request._
import unfiltered.response._
import unfiltered.netty.async.Plan
import unfiltered.netty.cycle.ThreadPool
import unfiltered.netty.ServerErrorResponse
import unfiltered.Async
import akka.actor.{Props, ActorSystem}
import akka.pattern.ask
import akka.util.Timeout
import com.mongodb.casbah.MongoConnection
import net.liftweb.json.JsonAST.{JNothing, JArray}
import com.typesafe.config.{ConfigFactory}
import scala.concurrent.Future
import scala.concurrent.duration._

object Traktor extends Plan with ThreadPool with ServerErrorResponse with Logging {
  val actorSystem = ActorSystem("TrActors")
  val movieDiscoveryActor = actorSystem.actorOf(Props[MovieDiscoveryActor], name = "MovieDiscoveryActor")
  val movieActor = actorSystem.actorOf(Props(new MovieActor(movieDiscoveryActor)), name = "MovieActor")

  val config = ConfigFactory.load("traktor").withFallback(ConfigFactory.load())
  val connection = MongoConnection(config.getString("db.host") + ":" + config.getString("db.port"))
  val db = connection(config.getString("db.name"))
  db.authenticate(config.getString("db.user"), config.getString("db.pass"))

  implicit val timeout = Timeout(10 second)
  implicit val formats = net.liftweb.json.DefaultFormats

  import scala.concurrent.ExecutionContext.Implicits.global

  def actorResponse[A](body: => Future[ResponseFunction[A]])(implicit responder: Async.Responder[A]) {
    body onComplete {
      case Success(rf) => responder.respond(rf)
      case Failure(e) =>
        e.printStackTrace()
        responder.respond(BadRequest)
    }
  }

  object Watched extends Params.Extract("watched", Params.first ~>
    { p: Option[String] => p.map(_.toBoolean) }
  )

  def intent = {
    case req =>
      implicit val responder = req
      req match {
        case GET(r) => r match {
          case Path(Seg(Nil)) => req.respond(Redirect("/index.html"))
          case Path(Seg("movies" :: Nil)) => actorResponse({ (movieActor ? AllMovies).mapTo[JArray].map { msg => Json(msg) } })
          case Path(Seg("movies" :: "pending" :: Nil)) => actorResponse({ (movieActor ? PendingMovies).mapTo[JArray].map { msg => Json(msg) } })
        }
        case POST(r) => r match {
          case Path(Seg("movies" :: "apply" :: Nil)) & Params(Watched(w)) => actorResponse(
            ({ movieActor ? ApplySearchResult(
              JsonBody(req).getOrElse(JNothing), watched = w)
            }).mapTo[Boolean].map {
              rsp => if (rsp) Ok else BadRequest
            })
          case Path(Seg("movies" :: "add" :: Nil)) => actorResponse(
            ({ movieActor ? AddMovies(
              JsonBody(req).map(body => body.extract[List[String]]).getOrElse(List())
            )}).mapTo[Boolean].map {
              rsp => if (rsp) Ok else BadRequest
            })
          case Path(Seg("movies" :: "delete" :: Nil)) => actorResponse(
            ({ movieActor ? DeleteMovies(
              JsonBody(req).map(body => body.extract[List[String]]).getOrElse(List())
            )}).mapTo[Boolean].map {
              rsp => if (rsp) Ok else BadRequest
            })
        }
        case _ => req.respond(BadRequest)
      }
  }

}

object Security extends Plan with ThreadPool with ServerErrorResponse with Logging {
  private def onFail(realm: String = "secret") = unfiltered.kit.Auth.defaultFail(realm)
  private val users = (u: String, p: String) => u == Traktor.config.getString("traktor.user") && p == Traktor.config.getString("traktor.pass")

  def intent = {
    case req & XForwardProto("http") => req.respond(HerokuHttpsRedirect(req, req.uri))
    case req@BasicAuth(u, p) if users(u, p) => Pass
    case req => req.respond(onFail())
  }
}

object XForwardProto extends StringHeader("x-forwarded-proto")

object HerokuHttpsRedirect {
  def apply[A, B](req: HttpRequest[A], path: String): ResponseFunction[B] = {
    val absolutepath = if (path.startsWith("/")) path else "/" + path
    req match {
      case XForwardProto(_) & Host(host) => Found ~> Location("https://%s%s".format(host, absolutepath))
      case _ => Redirect(absolutepath)
    }
  }
}

object Main extends App with Logging {
  val port = Properties.envOrElse("PORT", "8080").toInt
  logger.info("started on port:%d".format(port))
  unfiltered.netty.Http(port)
    .chunked(1048576)
    .handler(Security)
    .resources(new java.net.URL(getClass.getResource("/webapp/index.html"), "."))
    .handler(Traktor)
    .run()
}
