package traktor

import dispatch.classic._

object TraktClean extends App {

  val http = new Http

  val collectionRequest = :/("api.trakt.tv")/"user"/"library"/"movies"/"all.json"/
    Traktor.config.getString("trakt.apikey")/ Traktor.config.getString("trakt.user")/"min"
  val unlibraryRequest = :/("api.trakt.tv")/"movie"/"unseen"/Traktor.config.getString("trakt.apikey")

  http(collectionRequest >- {
    json => http(unlibraryRequest << ("{\"username\": \"" + Traktor.config.getString("trakt.user") +
      "\", \"password\": \"" + Traktor.config.getString("trakt.pass") + "\", \"movies\":" + json + "}",
      "application/json") >>> System.out)
  })

}
