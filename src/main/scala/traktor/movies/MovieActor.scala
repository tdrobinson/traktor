package traktor.movies

import akka.actor.{ActorRef, Actor}
import traktor.util.{DBCollection, Logging}
import akka.pattern.pipe
import com.mongodb.DBObject
import net.liftweb.json.JsonAST.{JObject, JArray, JValue}
import com.mongodb.casbah.Implicits._
import org.bson.types.ObjectId
import com.mongodb.casbah.commons.MongoDBObject

import scala.concurrent.Future

case object AllMovies
case object PendingMovies
case class ApplySearchResult(obj: JValue, watched: Boolean)
case class AddMovies(movies: List[String])
case class AddMoviesToDB(movies: List[JObject])
case class DeleteMovies(movies: List[String])
case class DeleteMoviesFromDB(movies: List[String])

class MovieActor(discoveryActor: ActorRef) extends Actor with DBCollection with Logging {
  val collection = db("movies")

  def receive = {
    case AllMovies => Future { listAll } pipeTo sender
    case PendingMovies => Future { listAllPending } pipeTo sender
    case ApplySearchResult(obj, watched) => Future { applySearchResult(obj, watched) } pipeTo sender
    case AddMovies(ms) => Future { addMovies(ms) } pipeTo sender
    case AddMoviesToDB(ms) => Future { addMoviesToDB(ms) } onFailure { case e => e.printStackTrace() }
    case DeleteMovies(ms) => Future { removeMovies(ms) } pipeTo sender
    case DeleteMoviesFromDB(ms) => Future { removeMoviesFromDB(ms) } onFailure { case e => e.printStackTrace() }
  }

  def listAll(implicit dbObject2JObject: DBObject => JValue) =
    JArray(collection.find().toList.map(db => dbObject2JObject(db)))

  def listAllPending(implicit dbObject2JObject: DBObject => JValue) =
    JArray(collection.find("results" $exists true).toList.map(db => dbObject2JObject(db)))

  def applySearchResult(obj: JValue, watched: Boolean)(implicit jValue2DBObject: JValue => DBObject) = {
    val result = jValue2DBObject(obj)
    collection.findOneByID(new ObjectId(result.removeField("movie_id").asInstanceOf[String])) match {
      case Some(movie) =>
        movie.removeField("results")
        result.foreach {
          case (key, value) => movie.put(key, value)
        }
        discoveryActor ! MovieCollectionAdd(movie.toString, watched)
        collection.save(movie)
        true
      case None => false
    }
  }

  def addMovies(ms: List[String]) = {
    try {
      val displayNames = ms.map(m => (m, m.substring(m.lastIndexOf('/') + 1, m.lastIndexOf('.'))))
      discoveryActor ! MovieSearch(displayNames)
      true
    } catch {
      case e: IndexOutOfBoundsException => false
    }
  }

  def removeMovies(ms: List[String]) = {
    try
    {
      val forRemoval = ms.map(m => collection.findOne(MongoDBObject("filename" -> m))).filter(m => {
        m.isDefined && !m.get.containsField("results")
      }).map(_.get.toString)
      discoveryActor ! MovieCollectionRemove(forRemoval)
      self ! DeleteMoviesFromDB(ms)
      true
    } catch {
      case _ : Throwable => false
    }
  }

  def addMoviesToDB(ms: List[JObject])(implicit jValue2DBObject: JValue => DBObject) {
    collection.insert(ms.map(jValue2DBObject):_*)
  }

  def removeMoviesFromDB(ms: List[String]) {
    ms.foreach(m => collection.remove(MongoDBObject("filename" -> m)))
  }
}
