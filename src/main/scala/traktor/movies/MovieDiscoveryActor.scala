package traktor.movies

import akka.actor.Actor
import traktor.util.{Logging, TraktRequests}
import akka.pattern.pipe
import dispatch.classic._
import java.net.URLEncoder
import net.liftweb.json._
import net.liftweb.json.Extraction.decompose
import net.liftweb.json.JsonAST.JArray
import net.liftweb.json.JsonAST.JDouble
import net.liftweb.json.JsonAST.JField
import net.liftweb.json.JsonAST.JNothing
import net.liftweb.json.JsonAST.JObject
import net.liftweb.json.JsonAST.JString
import traktor.Traktor
import scala.concurrent.Future

case class MovieSearch(titles: List[(String, String)])
case class MovieCollectionAdd(movie: String, watched: Boolean)
case class MovieCollectionRemove(movie: List[String])

class MovieDiscoveryActor extends Actor with TraktRequests with Logging {

  def receive = {
    case MovieSearch(titles) => Future { AddMoviesToDB(movieSearch(titles)) } pipeTo sender
    case MovieCollectionAdd(movie, watched) => Future { addMovieToCollection(movie, watched) } onFailure { case e => logger.error("Problem adding movie to collection: " + e.getMessage) }
    case MovieCollectionRemove(movie) => Future { removeMovieFromCollection(movie) } onFailure { case e => logger.error("Problem removing movie from collection: " + e.getMessage) }
  }

  def movieSearch(titles: List[(String, String)]) = titles.map { case (filename, title) =>
    //TODO this should be simpler but the trakt api is a bit average atm!
    val encoded = URLEncoder.encode(title.filter(c => c.isLetterOrDigit || c.isSpaceChar), "UTF-8")
    if (!encoded.contains('%'))
    {
      val response = makeRequest(:/(traktApi) / search / movies / apiKey / encoded).asInstanceOf[JArray]
      val results = response.arr.map(result => {
        val obj = keepUsefulFields(result.asInstanceOf[JObject])
        obj
      })
      JObject(List(JField("filename", JString(filename)), JField("results", JArray(results))))
    }
    else JObject(List(JField("filename", JString(filename)), JField("results", JArray(List()))))
  }

  //TODO refactor these methods!
  def addMovieToCollection(movieJson: String, watched: Boolean) = {
    val request =
      "{\"username\": \"" + Traktor.config.getString("trakt.user") + "\", " +
       "\"password\": \"" + Traktor.config.getString("trakt.pass") + "\", " +
       "\"movies\":[" + movieJson + "]}"

    makeRequest(:/(traktApi) / movie / library / apiKey << (request, "application/json"))
    if (watched) {
      makeRequest(:/(traktApi) / movie / seen / apiKey << (request, "application/json"))
    }
  }

  def removeMovieFromCollection(movieJson: List[String]) = {
    if (movieJson.nonEmpty)
    {
      makeRequest(:/(traktApi) / movie / unlibrary / apiKey <<
        ("{\"username\": \"" + Traktor.config.getString("trakt.user") + "\", " +
          "\"password\": \"" + Traktor.config.getString("trakt.pass") + "\", " +
          "\"movies\":[" + movieJson.reduceLeft(_ + ", " + _) + "]}", "application/json"))
    }
  }

  def keepUsefulFields(jObject: JObject) = {
    val transformed = jObject.transform {
      case imdbId@JField("imdb_id", _) => imdbId
      case title@JField("title", _) => title
      case JField("year", JInt(year)) => JField("year", JDouble(year.doubleValue()))
      case JField(_, _) => JNothing
      case x => x
    }
    parse(compact(render(transformed)))
  }

}
