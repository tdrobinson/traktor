package traktor.util

import dispatch.classic._
import net.liftweb.json.parse
import traktor.Traktor

trait TraktRequests {
  val traktApi = "api.trakt.tv"
  val apiKey = Traktor.config.getString("trakt.apikey")

  val search = "search"
  val movie = "movie"
  val seen = "seen"
  val library = "library"
  val unlibrary = "unlibrary"
  val movies = "movies.json"

  val http = new Http

  implicit val defaultExecutionContext = scala.concurrent.ExecutionContext.Implicits.global

  def makeRequest(request: Request) = http(request >- { in => parse(in) })
}
