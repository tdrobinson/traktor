package traktor.util

import traktor.Traktor
import com.mongodb.DBObject
import net.liftweb.json._
import com.mongodb.util.JSON


trait DBCollection {
  lazy val db = Traktor.db
  implicit val defaultExecutionContext = scala.concurrent.ExecutionContext.Implicits.global
  implicit val formats = net.liftweb.json.DefaultFormats
  implicit def DBObject2JValue(dbObject: DBObject) = parse(dbObject.toString)
  implicit def JValue2DBObject(jValue: JValue) = JSON.parse(compact(render(jValue))).asInstanceOf[DBObject]
}
