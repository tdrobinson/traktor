package traktor

import java.io.{FileFilter, File}
import com.typesafe.config.ConfigFactory
import dispatch.classic._
import net.liftweb.json._
import net.liftweb.json.Extraction.decompose
import net.liftweb.json.JsonAST.JString

object TraktorUpdate {
  import scala.collection.JavaConverters._

  val http = new Http

  def main(args: Array[String]) {
    val config = loadConfig(args(0))

    val moviePath = config.getString("traktor.movies")
    val masks = config.getStringList("traktor.masks").asScala.toList
    val host = config.getString("traktor.host")
    val port = config.getInt("traktor.port")
    val user = config.getString("traktor.user")
    val pass = config.getString("traktor.pass")

    val local = getLocalMovies(moviePath, masks)

    val remote = getTraktorMovies(host, port, user, pass)

    val toAdd = (local -- remote).toList
    val toDelete = (remote -- local).toList

    postMovies(host, port, user, pass, "add", toAdd)
    postMovies(host, port, user, pass, "delete", toDelete)
  }

  implicit def videoFileFilter(masks: List[String]): FileFilter = new FileFilter {
    def accept(pathname: File) = masks.exists(mask => pathname.getPath.endsWith(mask))
  }

  def loadConfig(configPath: String) = ConfigFactory.parseFile(new File(configPath))

  def getTraktorMovies(host: String, port: Int, username: String, password: String): Set[String] = {
    http(:/(host, port).as(username, password) / "movies" >- {
      json => parse(json) \\ "filename" \\ classOf[JString]
    }).toSet
  }

  def getLocalMovies(moviePath: String, masks: List[String])(implicit filter: List[String] => FileFilter) = new File(moviePath).listFiles(filter(masks)).map(_.getCanonicalPath).toSet

  def postMovies(host: String, port: Int, username: String, password: String, route: String, ms: List[String]) = {
    if (ms.nonEmpty)
    {
      http(:/(host, port).as(username, password) / "movies" / route
           <:< Map("Content-Type" -> "application/json; charset=UTF-8")
           << pretty(render(decompose(ms)(DefaultFormats)))
           >>> System.out
      )
    }
  }

}
