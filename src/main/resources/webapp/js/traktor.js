var traktor = angular.module('traktor', []);

traktor.filter('displayName', function() {
    return function(input) {
        var slash = input.lastIndexOf("/");
        var dot = input.lastIndexOf(".");
        return input.substring(slash + 1, dot);
    };
});

traktor.factory('movieBroadcast', function($rootScope) {
    var movieBroadcaster = {};

    movieBroadcaster.movie = null;
    movieBroadcaster.eventName = '';

    movieBroadcaster.broadcast = function(eventName, movie) {
        this.movie = movie;
        this.eventName = eventName;
        this.broadcastItem();
    }

    movieBroadcaster.broadcastItem = function() {
        $rootScope.$broadcast(this.eventName);
    }

    return movieBroadcaster;
});

var PendingMovieListCtrl = function($scope, $http, movieBroadcast) {
    $scope.pendingMovies = [];
    $scope.selectedMovie = null;
    $http.get('/movies/pending').success(function(data) {
        $scope.pendingMovies = data;
    });

    $scope.selectMovie = function(movie) {
        movieBroadcast.broadcast('movieSelectionChanged', movie);
    }

    $scope.$on('movieResultApplied', function() {
        var index = $scope.pendingMovies.indexOf(movieBroadcast.movie);
        if (index != -1) {
            $scope.pendingMovies.splice(index, 1);
            $scope.selectMovie($scope.pendingMovies[index]);
        }

    })

    $scope.$on('movieSelectionChanged', function() {
        $scope.selectedMovie = movieBroadcast.movie;
    })
};

var PendingMovieResultsCtrl = function($scope, $http, movieBroadcast) {
    $scope.selectedResults = [];
    $scope.movieIsSelected = false;
    $scope.noResults = true;
    $scope.fakeNoResults = true;

    $scope.$on('movieSelectionChanged', function() {
        if (movieBroadcast.movie) {
            $scope.selectedResults = movieBroadcast.movie.results;
            $scope.movieIsSelected = true;
            $scope.noResults = $scope.selectedResults.length === 0;
        }
        else {
            $scope.selectedResults = [];
            $scope.movieIsSelected = false;
            $scope.noResults = true;
        }
        $scope.fakeNoResults = $scope.noResults;
    });

    $scope.applySearchResult = function(result, watched) {
        result["movie_id"] = movieBroadcast.movie._id.$oid;
        $http.post('/movies/apply?watched=' + watched, result)
            .success(function() {
                movieBroadcast.broadcast('movieResultApplied', movieBroadcast.movie);
            })
            .error(function(data) {
                //TODO show message saying failure
            });
    }

    $scope.toggleFakeNoResults = function() {
        $scope.fakeNoResults = !$scope.fakeNoResults;
    }
};
