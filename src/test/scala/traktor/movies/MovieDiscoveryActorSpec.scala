package traktor.movies

import org.specs2.mutable._
import akka.testkit.{TestActorRef, TestKit}
import net.liftweb.json.JsonAST.{JNothing, JString, JField, JObject}
import akka.actor.ActorSystem

class MovieDiscoveryActorSpec extends TestKit(ActorSystem("test")) with Specification {

  val actor = TestActorRef(new MovieDiscoveryActor)

  "keep useful fields" should {
    "remove the junk" in {
      val obj = JObject(List(JField("title", JString("Mary Poppins")), JField("garbage", JString("I don't want to live here anymore!"))))
      val newObj = actor.underlyingActor.keepUsefulFields(obj)
      println(newObj)
      (newObj \ "garbage") mustEqual JNothing
      (newObj \ "title") mustEqual JString("Mary Poppins")
    }
  }


}
