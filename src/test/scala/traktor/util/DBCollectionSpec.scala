package traktor.util

import org.specs2.mutable._
import com.mongodb.casbah.commons.{MongoDBList, MongoDBObject}
import net.liftweb.json.JsonAST.{JArray, JField, JObject, JString}

class DBCollectionSpec extends Specification with DBCollection {

  "DBObject2JValue" should {
    "convert DBObject to JValue" in {
      val dbObj = MongoDBObject(("title", "Mary Poppins"), ("story", "Stuff Happens"))
      val converted = DBObject2JValue(dbObj)
      (converted \ "title") mustEqual JString("Mary Poppins")
      (converted \ "story") mustEqual JString("Stuff Happens")
    }
  }

  "JValue2DBObject" should {
    "convert JValue to DBObject" in {
      val jObject = JObject(List(JField("title", JString("Mary Poppins")), JField("story", JString("Stuff Happens")), JField("results", JArray(List(JObject(List(JField("title", JString("Result 1")))))))))
      val converted = JValue2DBObject(jObject)
      converted.get("title") mustEqual "Mary Poppins"
      converted.get("story") mustEqual "Stuff Happens"
      converted.get("results") mustEqual MongoDBList(MongoDBObject(("title", "Result 1")))
    }
  }

}
